STATE = {InFree, InWant, InRegister, InWait, InDembiec, InJured}
COMMANDS = {ACK, REQUEST, READY, START, DEPART}

ready wysyłane do wszystkich, jak proces odbierze ready to dodaje do swojej potencjalnej grupy i gdy jest w sekcji krytycznej i ma grupe = pojemosc to wysyła START
jezeli proces odbierze od kogos START gdy ma go w grupie to usuwa go z grupy i dodaje na blackliste. z blacklisty proces znika gdy wysle ACKa 

KOMUNIKACYJNY:

while(true):
    handle_clock()
    switch (state):
        case InFree ->
            pid, clock, tag = recv
            switch (tag):
                case REQUEST ->
                        if (pid in temp_queue):
                            temp_queue.remove(pid)
                            break;
                        wait_queue ++ pid
                        send(ACK, pid)

        case InWant ->
            pid, clock, tag = recv
            switch(tag):
                case REQUEST ->
                    if (pid in temp_queue):
                        temp_queue.remove(pid)
                        break;
                    wait_queue ++ pid
                    send(ACK, pid)

                case ACK ->
                    ack_count += 1

        case InWait ->
            pid, clock, tag = recv
            switch(tag):
                case START ->
                    change_state(InDembiec)

        case InDembiec ->

        case InJured ->
            pid, clock, tag = recv
            switch(tag):
                case REQUEST ->
                    send(ACK, pid)


GLOWNY:

while(true):
    switch(state):
        case InFree ->
            send(REQUEST, :all)
            change_state(InWant)
        case InWant ->
            if (ack_count > ALL_TOURISTS - GROUPSIZE * PRZEWODNIK_COUNT):
                if ((self.wait_queue_idx + 1) mod GROUPSIZE == 0):
                    send(START, wait_queue, wait_queue[self.idx - GROUPIZE : self.idx])
                    change_state(InDembiec)
                change_state(InWait)
        case InWait ->
        case InDembiec ->
            sleep(5)
            next_status = random(InFree, InJured)
            change_state(next_status)
            send(RELEASE, wait_queue[self.idx:])
            for uczestnik in start.uczestnicy:
                if ucZEstnik in wait_queue:
                    wait_queue.remove(uczesntnik)
                else:
                    temp_queue.add(uczestnik)

        case InJured ->
            sleep(5)
            change_state(InFree)
