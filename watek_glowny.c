#include "main.h"
#include "watek_glowny.h"

void mainLoop()
{
    srandom(rank);
    int tag;
    int perc;

    while (stan != InFinish) {
	switch (stan) {
	    case InFree: 
			debug("Jestem wolny wysylam requesty WG")
			initQueue(&temp_queue);
			initQueue(&queue);

			for (int i=0;i<=size-1;i++)
				if (i!=rank)
					sendPacket( 0, i, REQUEST);

			changeState(InWant);
			break;
	    case InWant:
			debug("Jestem InWant chce isc")
			break;

		case InWait:
			ackCount = 0;
			debug("InWait")
			debug("Zawiera: %d",containsPID(&queue, rank))
			if (containsPID(&queue, rank) % GROUP_SIZE == 0) {
				debug("Chyba będę szefem")
				packet_t *pkt = malloc(sizeof(packet_t));
				
				for (int i=GROUP_SIZE - 1; i > -1; i++){
					pkt->data[i] = queue.pids[rank - i];
				}

				for(int i=1; i< GROUP_SIZE; i++)
					sendPacket( pkt, queue.pids[rank - i], START);
				changeState(InDembiec);
			}
			break;

	    case InDembiec:		
			debug("Jest imba WG")
			sleep(5);
			int random = rand() % 100;
			if (random < 51) {
				changeState(InJured);
			} else {
				changeState(InFree);
			}
			
			int myIndex = containsPID(&queue, rank) + 1;
			for (int i=myIndex; i<queue.count; i++)
				sendPacket( 0, queue.pids[i], RELEASE);



			break;
		case InJured:
			debug("Jestem w HCP, mogę tego nie przeżyć")
			sleep(5);
			debug("Wychodze ze szpitala :)")
			changeState(InFree);
		break;

	    default: 
		break;
        }
        sleep(SEC_IN_STATE);
    }
}
