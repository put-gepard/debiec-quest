#include "main.h"
#include "watek_komunikacyjny.h"

/* wątek komunikacyjny; zajmuje się odbiorem i reakcją na komunikaty */
void *startKomWatek(void *ptr)
{   
    MPI_Status status;
    int is_message = FALSE;
    packet_t pakiet;
    /* Obrazuje pętlę odbierającą pakiety o różnych typach */
    while ( stan!=InFinish ) {
	    debug("czekam na recv");
        MPI_Recv( &pakiet, 1, MPI_PAKIET_T, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        
        pthread_mutex_lock(&clock_mutex);
        if (pakiet.ts > zegar) {
            zegar = pakiet.ts;
        }
        zegar++;
        pthread_mutex_unlock(&clock_mutex);
    
        switch ( stan ) {
            debug("%d", stan);
            case InFree:
                debug("Jestem wolny jak ptak!")
                switch (status.MPI_TAG) {
                    case REQUEST:
                        if (containsPID(&temp_queue, status.MPI_SOURCE) != -1) {
                            removePID(&temp_queue, status.MPI_SOURCE);
                        } else {
                            debug("dodaję %d", status.MPI_SOURCE)
                            addWaitQueue(&queue, status.MPI_SOURCE);
                            sendPacket( 0, status.MPI_SOURCE, ACK );
                        }
                        break;
                    default:
                    break;
                }
            break;

            case InWant:
                debug("Chce na dembiec")
                switch(status.MPI_TAG) {
                    case REQUEST:
                        if (containsPID(&temp_queue, status.MPI_SOURCE) != -1) {
                            removePID(&temp_queue, status.MPI_SOURCE);
                        } else {
                            debug("dodaję %d", status.MPI_SOURCE)
                            addWaitQueue(&queue, status.MPI_SOURCE);
                            sendPacket( 0, status.MPI_SOURCE, ACK );
                        }
                        break;
                    case ACK:
                        ackCount++;
                        if (ackCount >= allTourists - GROUP_SIZE * guideNumber) {
                            debug("Dostałem %d acków, thx", ackCount)
                            changeState(InWait);
                        }
                        break;
                    default:
                    break;
                }
            break;

            case InWait:
                debug("Czekam na dembiec")
                switch(status.MPI_TAG) {
                    case START:
                        changeState(InDembiec);
                        break;
                    default:
                    break;
                }
            break;

            case InJured:
                debug("Jestem ranny")
                switch(status.MPI_TAG) {
                    case REQUEST:
                        if (containsPID(&temp_queue, status.MPI_SOURCE) != -1) {
                            removePID(&temp_queue, status.MPI_SOURCE);
                        } else {
                            sendPacket( 0, status.MPI_SOURCE, ACK );
                        }
                        break;
                    default:
                    break;
                }
            break;

            default:
            break;
        }
    }
}
