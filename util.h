#pragma once

#ifndef UTILH
#define UTILH
#include "main.h"
#include <stdbool.h>

#define GROUP_SIZE 2

/* typ pakietu */
typedef struct {
    int ts;       /* timestamp (zegar lamporta */
    int src;  

    int data[GROUP_SIZE];     /* przykładowe pole z danymi; można zmienić nazwę na bardziej pasującą */
} packet_t;
/* packet_t ma trzy pola, więc NITEMS=3. Wykorzystane w inicjuj_typ_pakietu */


typedef struct {
    int *pids;
    int count;
    int capacity;
} PIDQueue;

#define NITEMS 3
#define MAX_PIDS 100

/* Typy wiadomości */
/* TYPY PAKIETÓW */
#define ACK     1
#define REQUEST 2
#define READY   3 
#define START   4 // ACK od tych co już czekają na grupe
#define RELEASE  5

    
extern MPI_Datatype MPI_PAKIET_T;
extern PIDQueue queue;
extern PIDQueue temp_queue;
void inicjuj_typ_pakietu();

/* wysyłanie pakietu, skrót: wskaźnik do pakietu (0 oznacza stwórz pusty pakiet), do kogo, z jakim typem */
void sendPacket(packet_t *pkt, int destination, int tag);

typedef enum {InFree, InWant, InWait, InDembiec, InJured, InFinish} state_t;
extern state_t stan;
extern pthread_mutex_t stateMut;

/* zmiana stanu, obwarowana muteksem */
void changeState( state_t );
void initQueue(PIDQueue *);
bool addWaitQueue(PIDQueue *, int);
bool removePID(PIDQueue *, int);
bool removeLastPID(PIDQueue *);
int containsPID(PIDQueue *, int);
int getPIDCount(PIDQueue *);
void freeQueue(PIDQueue *);
#endif
